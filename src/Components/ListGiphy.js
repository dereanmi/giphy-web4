import React from 'react'
import ItemGiphy from './ItemGiphy'
import { List } from 'antd'

function ListMovie(props) {
    return (
        <div style={{ minHight: '300 px' }}>
            <List
                pagination={{
                    pageSize: 40,
                }}
               
                grid={{ gutter: 10, column: 4 }}
                dataSource={props.items}
                renderItem={item => (
                    <List.Item>
                        <ItemGiphy item={item} />
                    </List.Item>
                )}
            />
        </div>
    )
}
export default ListMovie