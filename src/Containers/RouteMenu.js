import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListMovie from '../Components/ListGiphy';
import ListFavorite from '../Components/favorite/list';
import Profile from '../Components/profile/index';

function RouteMenu(props) {
    return (
        <Switch>
            <Route
                path="/home"
                exact
                render={() => {
                    return (
                        <ListMovie
                            items={props.items}
                        />
                    );
                }}
            />
            <Route path="/favorite" exact component={ListFavorite} />
            <Route path="/profile" exact component={Profile} />
            <Redirect from="/*" exact to="/" />
        </Switch>
    );
}

export default RouteMenu;